# h-craft

**H-Craft Championship fixed upstream for PowerPC64 architecture**

This repository contains the source-code for the game H-Craft Championship from Irrgheist, for more informatio please visit : http://www.irrgheist.com/hcraftsource.htm 

Media files could be downloaded from here : https://github.com/mzeilfelder/media_hc1
